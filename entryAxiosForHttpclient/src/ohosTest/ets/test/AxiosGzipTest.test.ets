/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the 'License');
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an 'AS IS' BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import hilog from '@ohos.hilog';
import { describe, expect, it } from '@ohos/hypium';
import { LOG } from '../../../main/ets/pages/common/Common'
import { axios, AxiosResponse, Dns, Logger, Response } from '@ohos/axiosForHttpclient'
import { GlobalContext } from '../testability/GlobalContext';
import { connection } from '@kit.NetworkKit';

export default function AxiosGzipTest() {
  describe('AxiosGzipTest', () => {
    const TAG = LOG.TAG;
    const DOMAIN = LOG.DOMAIN;

    it('GzipTest_https', 1, async (done: Function) => {
      try {
        let startTime = new Date().getTime();
        axios<string, AxiosResponse<string>, string | Object>({
          url: 'https://113.45.193.209:8001/gzip/getGzipData',
          method: 'get',
          responseType: 'string',
          caPath: 'mutualAuth_noPsw/ca.crt',
          context: GlobalContext.getContext().getObject('context') as Context
        }).then((res: AxiosResponse<string>) => {
          let endTime = new Date().getTime();
          let averageTime = (endTime - startTime) * 1000;
          hilog.info(DOMAIN, TAG, " GzipResp averageTime: " + averageTime + ' μs');
          hilog.info(DOMAIN, TAG, " GzipResp status: " + JSON.stringify(res.status));
          expect(res.data).assertContain('This is the data to be compressed');
          done();
        })
      } catch (err) {
        expect().assertFail()
      }
    })

    it('GzipTest_http', 1, async (done: Function) => {
      try {
        let startTime = new Date().getTime();
        axios<string, AxiosResponse<string>, string | Object>({
          url: 'http://113.45.193.209:8000/gzip/getGzipData',
          method: 'get',
          dns: new CustomDns(),
          responseType:'string'
        }).then((res: AxiosResponse<string>) => {
          let endTime = new Date().getTime();
          let averageTime = (endTime - startTime) * 1000;
          hilog.info(DOMAIN, TAG, " GzipResp averageTime: " + averageTime + ' μs');
          hilog.info(DOMAIN, TAG, " GzipResp status: " + JSON.stringify(res.status));
          expect(res.data).assertContain('This is the data to be compressed');
          done();
        })
      } catch (err) {
        expect().assertFail()
      }
    })
  })
}

export class CustomDns implements Dns {
  lookup(hostname: string): Promise<Array<connection.NetAddress>> {
    Logger.info('CustomDns hostname:', hostname);
    return new Promise((resolve, reject) => {
      connection.getAddressesByName(hostname).then((netAddress) => {
        Logger.info('CustomDns netAddress = ' + JSON.stringify(netAddress));
        resolve(netAddress);
      }).catch((err: Response) => {
        reject(err);
      });
    })
  }
}