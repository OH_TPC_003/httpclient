/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import hilog from '@ohos.hilog';
import { describe, beforeAll, it, expect, afterAll } from '@ohos/hypium'
import { axios, AxiosResponse, Cache } from '@ohos/axiosforhttpclient'
import { LOG } from '../../../main/ets/pages/common/Common'
import { GlobalContext } from '../testability/GlobalContext';

export default function AxiosCacheTest() {
  let cache: Cache.Cache;

  describe('AxiosCacheTest', () => {
    const TAG = LOG.TAG
    const DOMAIN = LOG.DOMAIN

    beforeAll(() => {
      let context = getContext();
      let hereCacheDir: string = context.cacheDir;
      cache = new Cache.Cache(hereCacheDir, 10 * 1024 * 1024, context);
    })

    afterAll(() => {
      cache.delete();
      cache.evictAll();
    })

    it('cache_request', 1, async (done: Function) => {
      // 发送请求
      let startTime = new Date().getTime()
      axios<string, AxiosResponse<string>, null>({
        url: 'https://113.45.193.209:8080/cache/e/tag',
        method: 'get',
        context: GlobalContext.getContext().getObject("context") as Context,
        caPath: 'mutualAuth_noPsw/ca.crt',
        cache: cache,
        responseType: 'string'
      }).then((res: AxiosResponse<string>) => {
        let endTime = new Date().getTime();
        let averageTime = endTime - startTime;
        hilog.info(DOMAIN, TAG, " cache_request averageTime: " + averageTime + ' μs');
        hilog.info(DOMAIN, TAG, " cache_request status: " + JSON.stringify(res.status));
        expect(res && res.status && res.status === 200)
          .assertTrue()
      })
      done()
    })
  })
}
