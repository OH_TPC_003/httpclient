/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {
  HttpClient,
  Request,
  Response,
  BusinessError,
  IOException,
  EventListener,
  HttpCall,
  RequestBody
} from '@ohos/httpclient';
import { Logger } from '@ohos/httpclient';

@Entry
@Component
struct EventListeners {
  scroller: Scroller | undefined = new Scroller();
  @State status: string | undefined = '';
  @State content: string | undefined = 'init';
  @State url: string | undefined = 'jsonplaceholder.typicode.com/posts';
  // @State url: string = '1.94.37.200:7070';
  eventListener: EventListener | undefined = new HttpEventListener()

  defaultClientListenerGet() {
    let client: HttpClient | undefined = new HttpClient.Builder()
      .addEventListener(this.eventListener)
      .build();

    let request: Request | undefined = new Request.Builder()
      .get(this.getUrl())
      .build();

    client.newCall(request).execute().then((result) => {
      Logger.info('result >>>>>' + JSON.stringify(result));
      if (result) {
        this.status = result.responseCode.toString();
      }
      if (result.result) {
        this.content = JSON.stringify(result.result);
      } else {
        this.content = JSON.stringify(result);
      }
      client = undefined;
      request = undefined;
    }).catch((error: BusinessError) => {
      this.status = error.code.toString();
      client = undefined;
      request = undefined;
    });
  }

  defaultClientListenerPost() {
    let client: HttpClient | undefined = new HttpClient.Builder()
      .addEventListener(this.eventListener)
      .build();

    let request: Request | undefined = new Request.Builder()
      .url(this.getUrl())
      .post(RequestBody.create({ 'name': 'Hello Kitty', 'status': 'sold' }))
      .build();

    client.newCall(request).execute().then((result) => {
      Logger.info('result >>>>>' + JSON.stringify(result));
      if (result) {
        this.status = result.responseCode.toString();
      }
      if (result.result) {
        this.content = JSON.stringify(result.result);
      } else {
        this.content = JSON.stringify(result);
      }
      client = undefined;
      request = undefined;
    }).catch((error: BusinessError) => {
      this.status = error.code.toString();
      client = undefined;
      request = undefined;
    });
  }

  aboutToDisappear(): void {
    this.scroller = undefined;
    this.status = undefined;
    this.content = undefined;
    this.url = undefined;
    this.eventListener = undefined;
  }

  getUrl() {
    return 'http://' + this.url
  }

  getResourceString(res: Resource) {
    return getContext().resourceManager.getStringSync(res.id)
  }

  build() {
    Column() {
      TextInput({ text: this.url, placeholder: $r('app.string.URL_address') })
        .fontSize(18)
        .onChange((value: string) => {
          if (!value) return;
          this.url = value;
        });
      Button($r('app.string.Customize_get'))
        .width('80%')
        .height(50)
        .fontSize(18)
        .fontColor(0xCCCCCC)
        .align(Alignment.Center)
        .margin(10)
        .onClick(() => {
          this.defaultClientListenerGet();
        });

      Button($r('app.string.Customize_post'))
        .width('80%')
        .height(50)
        .fontSize(18)
        .fontColor(0xCCCCCC)
        .align(Alignment.Center)
        .margin(10)
        .onClick(() => {
          this.defaultClientListenerPost();
        });

      Button($r('app.string.empty'))
        .width('50%')
        .height(50)
        .fontSize(18)
        .fontColor(0xCCCCCC)
        .align(Alignment.Center)
        .margin(10)
        .onClick(() => {
          this.content = '';
          this.status = '';
        });

      Text(this.getResourceString($r('app.string.Response_status')) + this.status)
        .fontSize(20);
      Scroll(this.scroller) {
        Text(this.getResourceString($r('app.string.Response_data')) + this.content)
          .fontSize(20);
      }
      .scrollable(ScrollDirection.Vertical) // 滚动方向纵向
      .scrollBarColor(Color.Gray) // 滚动条颜色
      .scrollBarWidth(10) // 滚动条宽度
      .edgeEffect(EdgeEffect.None);
    }
    .justifyContent(FlexAlign.Start)
    .width('100%')
    .height('100%');
  }
}


class HttpEventListener extends EventListener {
  protected startTime: number = 0;

  logWithTime(message: string) {
    const nosTime: number = new Date().getTime();
    if (message == 'callStart') {
      this.startTime = nosTime;
    }
    const elapsedTime: number = (nosTime - this.startTime) / 1000;
    Logger.info('自定义EventListener' + elapsedTime + ' ' + message);
  }

  callStart(call: HttpCall) {
    this.logWithTime('callStart');
  };

  dnsStart(call: HttpCall, domainName: string) {
    this.logWithTime('dnsStart');
  };

  dnsEnd(call: HttpCall, domainName: string, inetAddressList: []) {
    this.logWithTime('dnsEnd');
  };

  connectStart(call: HttpCall) {
    this.logWithTime('connectStart');
  };

  secureConnectStart(call: HttpCall) {
    this.logWithTime('secureConnectStart');
  };

  secureConnectEnd(call: HttpCall) {
    this.logWithTime('secureConnectEnd');
  };

  connectEnd(call: HttpCall) {
    this.logWithTime('connectEnd');
  };

  connectFailed(call: HttpCall) {
    this.logWithTime('connectFailed');
  }

  connectionAcquired(call: HttpCall) {
    this.logWithTime('connectionAcquired');
  }

  connectionReleased(call: HttpCall) {
    this.logWithTime('connectionReleased');
  }

  requestHeadersStart(call: HttpCall) {
    this.logWithTime('requestHeadersStart');
  }

  requestHeadersEnd(call: HttpCall, request: Request) {
    this.logWithTime('requestHeadersEnd');
  }

  requestBodyStart(call: HttpCall) {
    this.logWithTime('requestBodyStart');
  }

  requestBodyEnd(call: HttpCall, request: Request) {
    this.logWithTime('requestBodyEnd');
  }

  requestFailed(call: HttpCall, ioe: IOException) {
    this.logWithTime('requestFailed' + JSON.stringify(ioe));
  }

  responseHeadersStart(call: HttpCall) {
    this.logWithTime('responseHeadersStart');
  }

  responseHeadersEnd(call: HttpCall, response: Response) {
    this.logWithTime('responseHeadersEnd');
  }

  responseBodyStart(call: HttpCall) {
    this.logWithTime('responseBodyStart');
  }

  responseBodyEnd(call: HttpCall, response: Response) {
    this.logWithTime('responseBodyEnd');
  }

  responseFailed(call: HttpCall, ioe: IOException) {
    this.logWithTime('responseFailed' + JSON.stringify(ioe));
  }

  callEnd(call: HttpCall) {
    this.logWithTime('callEnd');
  }

  callFailed(call: HttpCall, ioe: IOException) {
    this.logWithTime('callFailed' + JSON.stringify(ioe));
  }
}