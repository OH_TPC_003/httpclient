/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import router from '@ohos.router';

@Entry
@Component
struct Index {
  build() {
    Scroll() {

      Column() {
        
        Flex({ direction: FlexDirection.Column }) {
          Button('Core')
            .width('80%')
            .height('100%')
            .fontSize(25)
            .fontColor(0xCCCCCC)
            .align(Alignment.Center)
            .margin(10)
            .onClick((event: ClickEvent) => {
              router.pushUrl({
                url: 'pages/core'
              })
            })
        }
        .height('10%')
        .width('100%')
        .padding(10)

        Flex({ direction: FlexDirection.Column }) {
          Button('Sample')
            .width('80%')
            .height('100%')
            .fontSize(25)
            .fontColor(0xCCCCCC)
            .align(Alignment.Center)
            .margin(10)
            .onClick((event: ClickEvent) => {
              router.pushUrl({
                url: 'pages/sample'
              })
            })
        }
        .height('10%')
        .width('100%')
        .padding(10)

        Flex({ direction: FlexDirection.Column }) {
          Button('UploadAndDownload')
            .width('80%')
            .height('100%')
            .fontSize(18)
            .fontColor(0xCCCCCC)
            .align(Alignment.Center)
            .margin(10)
            .onClick((event: ClickEvent) => {
              router.pushUrl({
                url: 'pages/uploadAndDownload'
              })
            })
        }
        .height('10%')
        .width('100%')
        .padding(10)

        Flex({ direction: FlexDirection.Column }) {
          Button('Auxiliary')
            .width('80%')
            .height('100%')
            .fontSize(25)
            .fontColor(0xCCCCCC)
            .align(Alignment.Center)
            .margin(10)
            .onClick((event: ClickEvent) => {
              router.pushUrl({
                url: 'pages/auxiliary'
              })
            })
        }
        .height('10%')
        .width('100%')
        .padding(10)

        Flex({ direction: FlexDirection.Column }) {
          Button('Crypto')
            .width('80%')
            .height('100%')
            .fontSize(25)
            .fontColor(0xCCCCCC)
            .align(Alignment.Center)
            .margin(10)
            .onClick((event: ClickEvent) => {
              router.pushUrl({
                url: 'pages/crypto'
              })
            })
        }
        .height('10%')
        .width('100%')
        .padding(10)

        Flex({ direction: FlexDirection.Column }) {
          Button('Encode_Decode_sample')
            .width('80%')
            .height('100%')
            .fontSize(25)
            .fontColor(0xCCCCCC)
            .align(Alignment.Center)
            .margin(10)
            .onClick((event: ClickEvent) => {
              router.pushUrl({
                url: 'pages/encode_decode_sample'
              })
            })
        }
        .height('10%')
        .width('100%')
        .padding(10)

        Flex({ direction: FlexDirection.Column }) {
          Button('Compression')
            .width('80%')
            .height('100%')
            .fontSize(25)
            .fontColor(0xCCCCCC)
            .align(Alignment.Center)
            .margin(10)
            .onClick((event: ClickEvent) => {
              router.pushUrl({
                url: 'pages/compression'
              })
            })
        }
        .height('10%')
        .width('100%')
        .padding(10)

        Flex({ direction: FlexDirection.Column }) {
          Button('sockets')
            .width('80%')
            .height('100%')
            .fontSize(25)
            .fontColor(0xCCCCCC)
            .align(Alignment.Center)
            .margin(10)
            .onClick((event: ClickEvent) => {
              router.pushUrl({
                url: 'pages/sockets'
              })
            })
        }
        .height('10%')
        .width('100%')
        .padding(10)

        Flex({ direction: FlexDirection.Column }) {
          Button('RetryAndFollowUp')
            .width('80%')
            .height('100%')
            .fontSize(25)
            .fontColor(0xCCCCCC)
            .align(Alignment.Center)
            .margin(10)
            .onClick((event: ClickEvent) => {
              router.pushUrl({
                url: 'pages/retry'
              })
            })
        }
        .height('10%')
        .width('100%')
        .padding(10)

        Flex({ direction: FlexDirection.Column }) {
          Button('http2')
            .width('80%')
            .height('100%')
            .fontSize(25)
            .fontColor(0xCCCCCC)
            .align(Alignment.Center)
            .margin(10)
            .onClick((event: ClickEvent) => {
              router.pushUrl({
                url: 'pages/http2'
              })
            })
        }
        .height('10%')
        .width('100%')
        .padding(10)

        Flex({ direction: FlexDirection.Column }) {
          Button($r('app.string.IP_Selection'))
            .width('80%')
            .height('100%')
            .fontSize(25)
            .fontColor(0xCCCCCC)
            .align(Alignment.Center)
            .margin(10)
            .onClick((event: ClickEvent) => {
              router.pushUrl({
                url: 'pages/dns'
              })
            })
        }
        .height('10%')
        .width('100%')
        .padding(10)

        Flex({ direction: FlexDirection.Column }) {
          Button($r('app.string.Custom_Unidirectional'))
            .width('80%')
            .height('100%')
            .fontSize(25)
            .fontColor(0xCCCCCC)
            .align(Alignment.Center)
            .margin(10)
            .onClick((event: ClickEvent) => {
              router.pushUrl({
                url: 'pages/unidirectionalCustomCertificate'
              })
            })
        }
        .height('10%')
        .width('100%')
        .padding(10)

        Flex({ direction: FlexDirection.Column }) {
          Button($r('app.string.Custom_Certificate'))
            .width('80%')
            .height('100%')
            .fontSize(25)
            .fontColor(0xCCCCCC)
            .align(Alignment.Center)
            .margin(10)
            .onClick((event: ClickEvent) => {
              router.pushUrl({
                url: 'pages/bidirectionalCustomCertificate'
              })
            })
        }
        .height('10%')
        .width('100%')
        .padding(10)

        Flex({ direction: FlexDirection.Column }) {
          Button($r('app.string.Certificate_verification'))
            .width('80%')
            .height('100%')
            .fontSize(25)
            .fontColor(0xCCCCCC)
            .align(Alignment.Center)
            .margin(10)
            .onClick((event: ClickEvent) => {
              router.pushUrl({
                url: 'pages/certificate_cert'
              })
            })
        }
        .height('10%')
        .width('100%')
        .padding(10)

        Flex({ direction: FlexDirection.Column }) {
          Button($r('app.string.skipRemoteValidation'))
            .width('80%')
            .height('100%')
            .fontSize(25)
            .fontColor(0xCCCCCC)
            .align(Alignment.Center)
            .margin(10)
            .onClick((event: ClickEvent) => {
              router.pushUrl({
                url: 'pages/skipRemoteValidation'
              })
            })
        }
        .height('10%')
        .width('100%')
        .padding(10)

        Flex({ direction: FlexDirection.Column }) {
          Button('responseData')
            .width('80%')
            .height('100%')
            .fontSize(25)
            .fontColor(0xCCCCCC)
            .align(Alignment.Center)
            .margin(10)
            .onClick((event: ClickEvent) => {
              router.push({
                url: 'pages/WidgetsPage'
              })
            })
        }
        .height('10%')
        .width('100%')
        .padding(10);

        Flex({ direction: FlexDirection.Column }) {
          Button($r('app.string.Request_queue'))
            .width('80%')
            .height('100%')
            .fontSize(20)
            .fontColor(0xFFFFFF)
            .align(Alignment.Center)
            .margin(10)
            .onClick((event: ClickEvent) => {
              router.pushUrl({
                url: 'pages/priorityQueue'
              })
            })
        }
        .height('10%')
        .width('100%')
        .padding(10)

        Flex({ direction: FlexDirection.Column }) {
          Button('requestGzip')
            .width('80%')
            .height('100%')
            .fontSize(20)
            .fontColor(0xFFFFFF)
            .align(Alignment.Center)
            .margin(10)
            .onClick((event: ClickEvent) => {
              router.pushUrl({
                url: 'pages/requestGzip'
              })
            })
        }
        .height('10%')
        .width('100%')
        .padding(10)

        Flex({ direction: FlexDirection.Column }) {
          Button('eventListener')
            .width('80%')
            .height('100%')
            .fontSize(25)
            .fontColor(0xCCCCCC)
            .align(Alignment.Center)
            .margin(10)
            .onClick((event: ClickEvent) => {
              router.push({
                url: 'pages/eventListener'
              })
            })
        }
        .height('10%')
        .width('100%')
        .padding(10)

        Flex({ direction: FlexDirection.Column }) {
          Button($r('app.string.Certificate_Lock'))
            .width('80%')
            .height('100%')
            .fontSize(25)
            .fontColor(0xCCCCCC)
            .align(Alignment.Center)
            .margin(10)
            .onClick((event: ClickEvent) => {
              router.pushUrl({
                url: 'pages/certificate_pinner'
              })
            })
        }
        .height('10%')
        .width('100%')
        .padding(10)

        Flex({ direction: FlexDirection.Column }) {
          Button($r('app.string.Set_proxy'))
            .width('80%')
            .height('100%')
            .fontSize(25)
            .fontColor(0xCCCCCC)
            .align(Alignment.Center)
            .margin(10)
            .onClick((event: ClickEvent) => {
              router.pushUrl({
                url: 'pages/Proxy'
              })
            })
        }
        .height('10%')
        .width('100%')
        .padding(10)

        Blank().height(150)
      }.width('100%').margin({ top: 5 })
    }.height('100%').scrollable(ScrollDirection.Vertical)
  }
}